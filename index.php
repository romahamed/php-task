<?php session_start();?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PHP Task</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <form id="myForm" method="POST" action="saveData.php" autocomplete="off">
            <h1>Register New User:</h1>
            <!-- One "tab" for each step in the form: -->
            <div class="tab">Personal Information:
                <p><input type="text" name="FName" placeholder="First name..." oninput="this.className = ''"></p>
                <p><input type="text" name="LName" placeholder="Last name..." oninput="this.className = ''"></p>
                <p><input type="tel" name="Telephone" placeholder="Telephone..." oninput="this.className = ''"></p>
            </div>

            <div class="tab">Address Information:
                <p><input type="text" name="Street" placeholder="Street..." oninput="this.className = ''"></p>
                <p><input type="number" name="HouseNumber" placeholder="House Number..." oninput="this.className = ''"></p>
                <p><input type="number" name="ZipCode" placeholder="Zip Code..." oninput="this.className = ''"></p>
                <p><input type="text" name="City" placeholder="City..." oninput="this.className = ''"></p>
            </div>

            <div class="tab">Payment Information:
                <p><input type="text" name="AccountOwner" placeholder="Account Owner..." oninput="this.className = ''"></p>
                <p><input type="text" name="IBAN" placeholder="IBAN..." oninput="this.className = ''"></p>
            </div>
            
            <div style="overflow:auto;">
                <div style="float:right;">
                    <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                    <button type="button" name="submit" id="nextBtn" onclick="nextPrev(1)">Next</button>
                    <button type="submit" id="sub" name="submit" style="display:none;">Submit</button>
                </div>
            </div>
            <!-- Circles which indicates the steps of the form: -->
            <div style="text-align:center;margin-top:40px;">
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
            </div>
        </form>
        <script src="script.js"></script>
    </body>
</html>
<?php session_destroy();?>