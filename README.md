**Multi Step Form with PHP and MySQL**

This task is based on php, MySQL and JavaScript to showcase my skills as junior backend php engineer.

---

## Description of Performance:

I will describe the whole task code performance optimization.

1. I avoid Unnecessary Use of **Global Variables**.
2. I used **ISSET** for make high performance.
3. Using **echo** as it's faster than any printing method in php like **print**.
4. I write secure form with good validation in html and js.
5. Till now The Code performance is very good and high.

---

## All required Things are done:

 just small partof code is not done (saving paymentDataId in my Database).

1. Multi steps form with HTML and JavaScript.
2. Pure php code that make loading faster.
3. View page for show success message and paymentDataId retrieved as json.
4. form consit of View One for Personal Info., View Two for Address Info., and View three for Payment Info.. 

---